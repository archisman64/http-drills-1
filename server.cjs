const http = require('http');
const crypto = require('crypto');

const { port } = require('./config');

http.createServer((req, res) => {
    const { method, url } = req;
    if (method === 'GET' && url === '/') {
      const htmlContent = `
        <!DOCTYPE html>
        <html>
          <head>
          </head>
          <body>
              <h1>Welcome to Archisman's website!</h1>
              <h2>You can try the following endpoints here:</h2>
              <ul>
                <li><a href="/html">/html</a></li>
                <li><a href="/json">/json</a></li>
                <li><a href="/uuid">/uuid</a></li>
                <li><a href="/status/200">/status/default</a></li>
                <li><a href="/delay/1">/delay/default</a></li>
              </ul>
          </body>
        </html>
      `;
      res.setHeader('Content-Type', 'text/html');
      res.end(htmlContent);
    }

    // 1)
    else if (method === 'GET' && url === '/html') {
        const htmlContent = `
          <!DOCTYPE html>
          <html>
            <head>
            </head>
            <body>
                <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
                <p> - Martin Fowler</p>
            </body>
          </html>
        `;
        res.setHeader('Content-Type', 'text/html');
        res.end(htmlContent);
    } else if (method === 'GET' && url === '/json') {
    // 2) 
        const jsonData = {
          slideshow: {
            author: 'Yours Truly',
            date: 'date of publication',
            slides: [
              {
                title: 'Wake up to WonderWidgets!',
                type: 'all',
              },
              {
                items: [
                  'Why <em>WonderWidgets</em> are great',
                  'Who <em>buys</em> WonderWidgets',
                ],
                title: 'Overview',
                type: 'all',
              },
            ],
            title: 'Sample Slide Show',
          },
        };
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(jsonData));
    } else if (method === 'GET' && url === '/uuid') {
    // 3)
    const generatedUuid = crypto.randomUUID();
    const responseData = { uuid: generatedUuid };

    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(responseData));

  } else if (method === 'GET' && url.startsWith('/status/')) {
    // 4)
    // console.log(url.split('/status/'))
    const statusCode = parseInt(url.split('/status/')[1]);
    let htmlContent;
    
    if(http.STATUS_CODES[statusCode]) {
      htmlContent = `
        <!DOCTYPE html>
        <html>
          <head>
          </head>
          <body>
              <h2>${statusCode}: ${http.STATUS_CODES[statusCode]}</h2>
          </body>
        </html>
      `;
      // console.log('found:', http.STATUS_CODES[statusCode]);
      res.statusCode = statusCode;
      res.setHeader('Content-Type', 'text/html');
      res.end(htmlContent);
    } else {
      htmlContent = `
        <!DOCTYPE html>
        <html>
          <head>
          </head>
          <body>
              <h2>Bad Request</h2>
              <h4>The code you requested is not a valid http status code.</h4>
          </body>
        </html>
      `;
      
      res.writeHead(400, 'Content-Type', 'text/html');
      res.end(htmlContent);
    }
    
  } else if (method === 'GET' && url.startsWith('/delay/')) {
    // 5)
    const delayInSeconds = Number(url.split('/delay/')[1]);
    let htmlContent;
    console.log(typeof delayInSeconds);
    if((delayInSeconds!== 0 && !delayInSeconds) || delayInSeconds < 0 || delayInSeconds > 10) {
      htmlContent = `
        <!DOCTYPE html>
        <html>
          <head>
          </head>
          <body>
              <h3>Please enter a valid delay</h3>
              <h2>Valid Range: 0 - 10 seconds</h2>
          </body>
        </html>
      `;

      res.setHeader('Content-Type', 'text/html');
      res.statusCode = 400;
      res.end(htmlContent);
    } else {
      setTimeout(() => {
          htmlContent = `
            <!DOCTYPE html>
            <html>
              <head>
              </head>
              <body>
                  <h3>THIS IS A DELAYED RESPONSE BY ${delayInSeconds} SECS!</h3>
              </body>
            </html>
          `;
          res.setHeader('Content-Type', 'text/html');
          res.statusCode = 200;
          res.end(htmlContent);
      }, delayInSeconds * 1000);
    }
  } else {
    const htmlContent = `
      <!DOCTYPE html>
      <html>
        <head>
        </head>
        <body>
            <h2>Requested endpoint is not supported!</h2>
            <h3>Please enter a valid endpoint.</h3>
        </body>
      </html>
    `;
    res.setHeader('Content-Type', 'text/html');
    res.statusCode = 404;
    res.end(htmlContent);
  }
}).listen(port, () => {
    console.log('server is running!');
});